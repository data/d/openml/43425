# OpenML dataset: Alcohol-Consumption-in-Russia-(1998-2016)

https://www.openml.org/d/43425

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
This is Alcohol Consumption in Russia (1998-2016) Dataset. It contains values of consumption for wine, beer, vodka, brandy and champagne.
Content
Dataset has 1615 rows and 7 columns. Keys for columns:

"year" - year (1998-2016)
"region" - name of a federal subject of Russia. It could be oblast, republic, krai, autonomous okrug, federal city and a single autonomous oblast
"wine" - sale of wine in litres by year per capita
"beer" - sale of beer in litres by year per capita
"vodka" - sale of vodka in litres by year per capita
"champagne" - sale of champagne in litres by year per capita
"brandy" - sale of brandy in litres by year per capita

Acknowledgements
 (UIISS) - Unified interdepartmental information and statistical system
Inspiration
You can analyze the relationships between various years, find best regions by each feature and compare them.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43425) of an [OpenML dataset](https://www.openml.org/d/43425). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43425/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43425/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43425/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

